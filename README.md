# Tinkoff performance workshop Cheat-Sheet

В этой шпаргалке приведены шаги для предварительной подготовки к работе и для самостоятельной работы с материалами.

## Содержание

* [Подготовка](#подготовка)
  * [Установка Docker](#установка-docker)
  * [Загрузка образов Docker](#загрузка-образов-docker)
  * [Установка SBT](#установка-sbt)
  * [Установка IntellijIDEA и Scala plugin](#установка-intellijidea-и-scala-plugin)
  * [Проверка успешной установки](#проверка-успешной-установки)
* [Развернуть окружение для тестов](#развернуть-окружение-для-тестов)
  * [Установка](#установка)
  * [Настройка](#настройка)
* [Подготовить скрипты для тестов](#подготовить-скрипты-для-тестов)
* [Подгтовить пайплайн](#подготовить-пайплайн)
* [Запустить тест](#запустить-тест)

## Подготовка
Рекомендуется выполнить следующие шаги до начала воркшопа.

### Установка Docker

[Общие инструкции по установке docker](https://docs.docker.com/get-docker/)

<details>
<summary>Как установить Docker на Windows</summary>

* Рекомендуется [установить WSL 2.0](https://docs.microsoft.com/en-us/windows/wsl/install-win10) и далее следовать инструкции по установке docker для linux
* Либо [установить docker для Windows](https://docs.docker.com/docker-for-windows/install/). Не подходит для Windows Home, в остальных случаях могут возникать проблемы с работой.

</details>

<details>
<summary>Как установить Docker на Linux/WSL 2.0</summary>

Выполните действия из инструкций последовательно:
 * [Установить docker](https://docs.docker.com/engine/install/ubuntu/)
 * [Выполнять команды docker без sudo](https://docs.docker.com/engine/install/linux-postinstall/)
 * [Установить docker-compose](https://docs.docker.com/compose/install/). Обратите внимание, docker-compose устанавливается отдельно.

</details>

<details>
<summary>Как установить Docker на Mac</summary>

* Следовать [инструкции](https://docs.docker.com/docker-for-mac/install/)

</details>

### Загрузка образов Docker

* Скачать образы для окружения:

```shell script
git clone https://gitlab.com/tinkoffperfworkshop/part-1/gatling-sandbox.git
cd gatling-sandbox
docker-compose pull
```

* Скачать образ для запуска тестов:

```shell script
docker pull hseeberger/scala-sbt:11.0.8_1.3.13_2.12.12
```

### Установка SBT

**Примечание**:
Воркшоп предусматривает работу без установки SBT, с использованем готовых материалов.
Если не получилось установить SBT, можно отложить этот шаг до начала самостоятельной работы с Gatling.
Также можно использовать в работе [другую систему сборки](https://gatling.io/docs/current/installation/#using-a-build-tool).

* Для работы SBT требуется Java JDK, [скачать и установить можно по ссылке](https://adoptopenjdk.net/installation.html), следуя инструкции для своей платформы. Рекомендуется версия JDK 11.
* Общая инструкция по установке SBT [доступна по ссылке](https://www.scala-sbt.org/1.x/docs/Setup.html), [скачать SBT для Windows](https://www.scala-sbt.org/download.html). Рекомендуется версия SBT >1.3.9.

### Установка IntellijIDEA и Scala plugin
**Примечание**:
Воркшоп предусматривает работу без установки IntellijIDEA и Scala plugin, можно использовать любой текстовый редактор.
IDEA рекомендуется для дальнейшей самостоятельной работы с Gatling.

* [Скачать и установить IntellijIDEA Community](https://www.jetbrains.com/ru-ru/idea/download/).
* [Установить Scala plugin](https://plugins.jetbrains.com/plugin/1347-scala). [Как установить plugin в IntellijIDEA](https://www.jetbrains.com/help/idea/managing-plugins.html#repos)

### Проверка успешной установки

<details>
<summary>Проверить что Docker и Docker-compose успешно установлены</summary>

```shell script
docker --version
>Docker version 19.03.13, build 4484c46d9d
docker-compose --version
>docker-compose version 1.27.4, build 40524192
```

При возникновении проблем:

Linux:
Проверить что docker запущен (зависит от OS и настроек).

Вариант 1:
```shell script
sudo service docker status # проверить статус
sudo service docker start # запустить процесс
```
Варинат 2:
```shell script
sudo systemctl status docker # проверить статус
sudo systemctl start docker # запустить процесс
```

Windows (без WSL 2.0):
Проверить в трее статус сервиса, запустить/перезапустить в случае необходимости.

</details>

<details>
<summary>Проверить что все контейнеры загружены и готовы к работе</summary>

Выполнить в консоли:

```shell script
docker images
>REPOSITORY                          TAG                     IMAGE ID            CREATED             SIZE
>prom/prometheus                     v2.22.0                 7adf5a25557b        7 days ago          168MB
>influxdb                            1.8                     c15aefdd926b        9 days ago          307MB
>grafana/grafana                     7.2.1                   2f17bd84b75d        2 weeks ago         180MB
>hseeberger/scala-sbt                11.0.8_1.3.13_2.12.12   04ad0fd234ec        2 weeks ago         990MB
>gitlab/gitlab-runner                v13.4.1                 c12323e3103b        3 weeks ago         483MB
>portainer/portainer                 1.24.1                  62771b0b9b09        3 months ago        79.1MB
>timberio/vector                     0.10.0-alpine           2ebd263245dd        3 months ago        80.7MB
>grafana/loki                        1.5.0                   8646eadeb75e        5 months ago        183MB
>gcr.io/google-containers/cadvisor   v0.34.0                 d24b7db72c99        14 months ago       185MB
```

</details>

<details>
<summary>Проверить что Java установлена и работает</summary>

Подходит любая Java 11 версии:
```shell script
java --version
>openjdk 11.0.9 2020-10-20
>OpenJDK Runtime Environment (build 11.0.9+11-Ubuntu-0ubuntu1.20.04)
>OpenJDK 64-Bit Server VM (build 11.0.9+11-Ubuntu-0ubuntu1.20.04, mixed mode, sharing)
```

</details>

<details>
<summary>Проверить что SBT установлена и работает</summary>

```shell script
sbt --version
>sbt version in this project: 1.4.1
>sbt script version: 1.4.1
```

</details>

## Развернуть окружение для тестов

### Установка

Окружение для тестов разворачивается с использованием docker-compose.

Конфигурация окружения [хранится в репозитории gatling-sandbox](https://gitlab.com/tinkoffperfworkshop/part-1/gatling-sandbox).

**Примечания**:
* Окружение использует следующие порты, необходимо их освободить, либо заменить порт в файле docker-compose.yml на другой (>20000, например):
`2003, 3000, 3100, 8000, 8080, 8086, 9000, 9090`
* docker-compose создает сеть `gatling-sandbox`, к ней можно подключать другие контейнеры, указав сеть при запуске:
```shell script
docker run ... --network=gatling-sandbox ...
```
* Для работы рекомендуется иметь свободными минимум **4Гб RAM и 2CPU**

**Шаги для запуска окружения**:

* Клонировать репозиторий, если этот шаг пропущен в подготовке к воркшопу
```shell script
git clone https://gitlab.com/tinkoffperfworkshop/part-1/gatling-sandbox.git
```

* Перейти в директорию с проектом, если этот шаг пропущен в подготовке к воркшопу
```shell script
cd gatling-sandbox
```

* Поднять контейнеры с окружением
```shell script
docker-compose up -d
```

<details>
<summary>Пример успешного запуска</summary>

```shell script
$ docker-compose up -d
Creating network "gatling-sandbox" with the default driver
Creating volume "influx-data" with default driver
Creating volume "grafana-data" with default driver
Creating volume "portainer-data" with default driver
Creating volume "gitlab-runner-config" with default driver
Creating influxdb      ... done
Creating portainer     ... done
Creating gitlab-runner ... done
Creating cadvisor      ... done
Creating loki          ... done
Creating grafana       ... done
Creating vector        ... done
Creating prometheus    ... done
```

</details>

* Проверить что все контейнеры поднялись:
```shell script
docker-compose ps
```

<details>
<summary>Пример успешного запуска</summary>

```shell script
$ docker-compose ps
    Name                   Command                  State                           Ports
--------------------------------------------------------------------------------------------------------------
cadvisor        /usr/bin/cadvisor -logtostderr   Up (healthy)   0.0.0.0:8080->8080/tcp
gitlab-runner   /usr/bin/dumb-init /entryp ...   Up
grafana         /run.sh                          Up             0.0.0.0:3000->3000/tcp
influxdb        /entrypoint.sh influxd           Up             0.0.0.0:2003->2003/tcp, 0.0.0.0:8086->8086/tcp
loki            /usr/bin/loki -config.file ...   Up             0.0.0.0:3100->3100/tcp
portainer       /portainer -H unix:///var/ ...   Up             0.0.0.0:8000->8000/tcp, 0.0.0.0:9000->9000/tcp
prometheus      /bin/prometheus --config.f ...   Up             0.0.0.0:9090->9090/tcp
vector          /usr/local/bin/vector            Up             
```

</details>

### Настройка

* Grafana. Сервис для визуализации метрик/логов.

Открыть в браузере: [localhost:3000](http://localhost:3000/).
По-умолчанию установлен **логин/пароль: admin/admin**. При первом входе потребуется изменить пароль на любой другой.
После входа можно увидеть заранее установленные дашборды,
загрузить другие можно используя импорт и id дашборда с [официального сайта](https://grafana.com/grafana/dashboards). 

* Portainer. Сервис для управления docker-контейнерами на хосте.
В воркшопе не используется, добавлен для удобства самостоятельной работы.

Открыть в браузере: [localhost:9000](http://localhost:9000/).
При первом запуске попросит ввести **логин/пароль** длиной больше 8 символов: **admin/adminadmin**.

## Подготовить скрипты для тестов

Подготовим скрипт для [одного из методов httpbin](https://httpbin.org/).

Развернем приложение в контейнере:
```shell script
docker run --rm -d -p 80:80 --network=gatling-sandbox --name=httpbin kennethreitz/httpbin
```

Открыть в браузере [http://localhost](http://localhost)

### Генерация проекта Gatling из шаблона

**Примечание:**
В случае затруднений можно переходить к следующему шагу, [сгенерированный проект доступен в репозитории](https://gitlab.com/tinkoffperfworkshop/part-1/gatling-template).


Шаблон [доступен на GitHub](https://github.com/TinkoffCreditSystems/gatling-template.g8/tree/v0.2.3).

* В консоли выполнить команду
```shell script
sbt new TinkoffCreditSystems/gatling-template.g8
```
При запросе ввести переменные нажимать enter, это сгенерирует проект со значениями по-умолчанию.

<details>
<summary>Пример успешной генерации</summary>

```shell script
sbt new TinkoffCreditSystems/gatling-template.g8
[info] Loading settings for project global-plugins from gpg.sbt ...
[info] Loading global plugins from ~/.sbt/1.0/plugins
[info] Set current project to gatling (in build file:/gatling/)
[info] Set current project to gatling (in build file:/gatling/)

Creates a Gatling project with sample performance test.

name [myservice]:
package [ru.tinkoff.load]:
scala_version [2.12.12]:
sbt_version [1.3.13]:
gatling_version [3.4.1]:
sbt_gatling_version [3.2.1]:
gatling_picatinny_version [0.6.1]:

Template applied in /gatling/./myservice
```

</details>

* Открыть проект в любой IDE или воспользоваться текстовым редактором. Рекомендуется [IntellijIDEA](https://www.jetbrains.com/ru-ru/idea/) с установленным [плагином Scala](https://plugins.jetbrains.com/plugin/1347-scala).
[Как установить plugin в IntellijIDEA](https://www.jetbrains.com/help/idea/managing-plugins.html#repos)

Открыть проект в IntellijIDEA: `File -> Open`, перейти в директорию с проектом и выбрать файл `build.sbt`.

Далее произойдет загрузка зависимостей и индексация проекта.

* После загрузки проекта предлагается ознакомиться со структурой проекта

<details>
<summary>Структура проекта</summary>

![project-structure](img/gatling-project-structure.png "gatling project structure")

1. директория `project` хранит настройки SBT плагинов, сборки и зависимости проекта.
2. `src/test/resources` хранит ресурсы и настройки теста. 
Файл `gatling.conf` предназначен для настройки Gatling, `logback.xml` для настройки логирования, 
`simulation.conf` для хранения любых параметров симуляции. 
Сюда можно положить тестовые данные, которые будут использоваться в скриптах.
3. `src/test/scala/../cases` располагается описание отдельных запросов (кейсов) описанных на Gatling DSL.
4. `src/test/scala/../scenarios` запросы (кейсы), скомпонованные в цепочки вызовов/сценарии на Gatling DSL.
5. `src/test/scala/../{имя сервиса}` тут хранятся общие настройки для всех тестов, например, описание HTTP protocol.
6. `src/test/scala/../` настройки симуляций (тест-планы), шаги подачи нагрузки, описанные на Gatling DSL.
7. в корне проекта лежит файл `build.sbt`, в нем описываются шаги сборки проекта и перечисленны ссылки на зависимости.

</details>

* Внесение изменений в проект

Указываем базовый URL для тестов, изменения в файле `src/test/resources/simulation.conf`

Изменяем запрос в файле `src/test/scala/ru/tinkoff/load/myservice/cases/GetMainPage.scala`

Запускаем Debug симуляцию локально.

<details>
<summary>Как запустить тест в IDEA</summary>

`"gatling:testOnly ru.tinkoff.load.myservice.Debug"`

![how-to-run](img/run-test-idea.png "how-to-run-gatling")

</details>

<details>
<summary>Как запустить тест из консоли</summary>

В корне проекта выполнить:
```shell script
sbt "gatling:testOnly ru.tinkoff.load.myservice.Debug"
```

</details>


Убираем прокси из Debug симуляции в файле `src/test/scala/ru/tinkoff/load/myservice/Debug.scala`

* Создаем группу проектов и репозиторий в Gitlab

Открываем в браузере [https://gitlab.com/](https://gitlab.com/), регистрируемся или входит под своей учетной записью.

Открываем [https://gitlab.com/dashboard/groups](https://gitlab.com/dashboard/groups), создаем группу нажав кнопку New group. Указываем Group name - любое уникальное название.

Внутри группы создаем проект, действия такие же как при создании группы, уникальное имя проекта не требуется.

В созданном проекте есть инструкция как добавить существующий проект в git, выполняем команды локально в консоли в директории проекта.


**Для тех кто не успел создать проект или не смог запустить SBT**:
Зайти в проект [https://gitlab.com/tinkoffperfworkshop/part-1/gatling-template](https://gitlab.com/tinkoffperfworkshop/part-1/gatling-template) и нажать Fork в правом верхнем углу.
После этого указать свою созданную группу в gitlab для создания форка репозитория.

## Подготовить пайплайн

* Шаблон gitlab-ci пайплайна уже подготовлен и доступен в репозитории [https://gitlab.com/tinkoffperfworkshop/part-1/perftest-pipeline-template](https://gitlab.com/tinkoffperfworkshop/part-1/perftest-pipeline-template).
Пайплайн включает два шага: запуск теста и генерация отчета. Также gitlab автоматически добавит третий шаг с публикацией отчета.

* Для подключения шаблона в свой репозиторий необходимо в корне проекта gatling (рядом с `build.sbt`) создать файл `.gitlab-ci.yml`.

Содержимое файла `.gitlab-ci.yml`:
```yaml
# Подключить шаблонный пайплайн
include:
  - project: 'tinkoffperfworkshop/part-1/perftest-pipeline-template'
    ref: 'v0.1.0'
    file: '.performance-test-pipeline.yml'
# Переопределить переменные из шаблона или добавить новые
variables:
  SIMULATION: "Debug"
  baseUrl: "http://httpbin"
```

* Добавить изменения в репозиторий с проектом gatling. В корне проекта в консоли выполнить:
```shell script
git add .
git commit -m 'add .gitlab-ci.yml'
git push
```

* Перейти в созданную группу в gitlab, зайти в настройки агентов группы: `Settings -> CI/CD -> Runners`

* Выключить shared runners, если подключены. Нажать `Disable shared runners`

* В разделе `Set up a group Runner manually` найти п.3 и скопировать предложенный токен.

* Подставить токен в команду после registration-token и выполнить локально в консоли 

```shell script
docker run --rm -v gitlab-runner-config:/etc/gitlab-runner --network=gatling-sandbox gitlab/gitlab-runner:v13.4.1 register \
   --non-interactive --url "https://gitlab.com/" --registration-token "${PROJECT_REGISTRATION_TOKEN}" \
   --executor "docker" --docker-image alpine:latest --description "docker-runner" --tag-list "docker,aws" \
   --run-untagged="true" --locked="false" --access-level="not_protected"  --docker-network-mode="gatling-sandbox"
```

* Проверить что ранер появился в настройках группы `Settings -> CI/CD -> Runners` и доступен (зеленый индикатор)

## Запустить тест

* Развернуть приложение.

* В gitlab проекте gatling перейти в раздел `CI/CD -> Pipelines`.

* Запустить Job `test`. Первый запуск будет дольше, потому что SBT зависимости еще не закешированы, последующие запуски будут быстрее.

* Посмотреть в [grafana](http://localhost:3000) метрики gatling.

* Остановить контейнер с приложением, посмотреть логи с ошибками в [grafana](http://localhost:3000).
```shell script
docker container stop httpbin
```

* После завершения теста в gitlab запустить Job `report`, посмотреть артефакты.

* Автоматически выполнится публикация отчета в gitlab pages. Посмотреть путь можно в настройках проекта: `Settings -> Pages`.

